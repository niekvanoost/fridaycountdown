<!DOCTYPE html>
<!--[if (gt IE 9)|!(IE)]><!--><html lang='nl' class='no-js'><!--<![endif]-->
<head>
	<meta charset='UTF-8' />
	<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
	<meta http-equiv="refresh" content="120">
	<title>Home - Vlotmibo</title>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/countdown.js"></script>
	<script type="text/javascript">
		$(function () {
			$('#js-news').ticker({
				controls: false,
				titleText: 'RSS',
			});	
		});
	</script>
	<style>
	html { 
  background: url(images/background/<?php echo rand(6,11); ?>.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}

		
		.myBackground {
  opacity: .8;
}


  .row {
      position: relative;
  }

  .bottom {
    position: absolute;
    bottom: 0;
	width
  }
	</style>
	
</head>
<body>
		
		<div class="container myBackground">
		
		<div class="row bottom">
		<div class="jumbotron">
		<h1>It's the last countdown...</h1>
		<script>
			for (var x=new Date(); x.getDay()!=5; x.setDate(x.getDate()+1)){}
				var nextDay = x.getDate();
				var currentMonth = x.getMonth()+1;
				var currentYear = x.getFullYear();
			var myCountdownTest = new Countdown({
				year	: currentYear,
				month	: currentMonth, 
				day		: nextDay,
				hour	: 16,
				minute	: 00,
				width	: 500, 
				height	: 100
			});
		</script>

	
	</div>
	</div>
	</div>
	</div>

		
</body>
</html>