# README #

Countdown naar de volgende 16.00 uur op vrijdag.

### Eigenschappen ###

* Telt af naar 16.00 uur op een vrijdag
* Willekeurige achtergrond bij elke refresh
* Bootstrap 

### Set-up ###

* Clone deze repo naar een map waar je IIS of andere webserver naar kijkt
* Plaats achtergronden in de map /images/background/ met een oplopend nummer
* Plaats op regel 24 in de index.php de maximale count van de 'rand' functie aan.

```
#!php
<?php echo rand(6,10); ?>

```


### Known Bugs ###

* De teller begint pas opnieuw met tellen als het de vrijdag weer voorbij is.